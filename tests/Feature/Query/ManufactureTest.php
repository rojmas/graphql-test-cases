<?php

namespace Tests\Feature\Query;

use App\Models\Manufactor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ManufactureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    public function test_fetch_manufacure_but_record_not_found()
    {
        $query = '{
            manufactures{
                id
                name
            }
        }';

        $expected = [
            'data' => [
                'manufactures' => [],
            ],
        ];

        $this->returnResult($query, $expected);
    }


    public function test_fetch_manufacture_details_sucessfully()
    {
        Manufactor::create([
            "name" => $this->faker->name(),
            "description" => $this->faker->name(),
        ]);

        $query = '{
                manufactures{
                    id
                    name
                }
            }';

        $expected = [
            "data" => [
                "manufactures" => [
                    ["id", "name",]
                ]
            ]
        ];
        
        $this->returnResult($query, $expected);
    }
}
