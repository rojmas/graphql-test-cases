<?php

namespace App\GraphQL\Types;

use App\Models\Manufactor;
use App\Models\ManufactureModelImages as ModelsManufactureModelImages;
use App\Models\User;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ManufactureModelImages extends GraphQLType
{
    protected $attributes = [
        'name'          => 'manufacture_model_images',
        'description'   => 'A Manufacture Model Images',
        'model'         => ModelsManufactureModelImages::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the manufacture',
            ],
            'path' => [
                'type' =>  Type::nonNull(Type::string()),
                'description' => 'The images of the  of the manufacture',
            ],
        ];
    }
}
