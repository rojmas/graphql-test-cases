<?php

namespace App\GraphQL\Types;

use App\Models\Post;
use App\Models\User;
use Rebing\GraphQL\Support\Facades\GraphQL;
// use GraphQL\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class PostType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'post',
        'description'   => 'A Posts',
        // Note: only necessary if you use `SelectFields`
        'model'         => Post::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the user',
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of user',
            ],
            'content' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The email of user',
            ],

            'user' => [
                'type' => GraphQL::type('user'),
                'description' => 'a user who can create the post',
            ]
        ];
    }
}
