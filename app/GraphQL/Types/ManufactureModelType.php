<?php

namespace App\GraphQL\Types;

use App\Models\Manufactor;
use App\Models\ManufactorModel;
use App\Models\User;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ManufactureModelType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'manufacture_model',
        'description'   => 'A Manufacture Model',
        'model'         => ManufactorModel::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the manufacture',
            ],
            'model_name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of manufacture',
            ],
            'model_description' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The description of manufacture',

            ],
            'model_img' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The Model',

            ],
            // //reletionship for manufacture model have one manufactor
            'manufacture' => [
                'type' => GraphQL::type('manufacture'),
                'description' => 'manufacture model have one manufacures"s ',
            ],

            //reletinship for many iamges upload to the manufacure model
            'images' => [
                'type' =>  Type::listOf(GraphQL::type('manufacture_model_images')),
                'description' => 'manufacture model images by id',
            ],
        ];
    }

    // You can also resolve a field by declaring a method in the class
    // with the following format resolve[FIELD_NAME]Field()
    // protected function resolveEmailField($root, array $args)
    // {
    //     return strtolower($root->email);
    // }
}
