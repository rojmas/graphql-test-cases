<?php
namespace App\GraphQL\Types;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AuthType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Auth',
        'description' => 'Usre login',
        'model' => User::class
    ];

    public function fields(): array
    {
        return [
            'token' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'User access token'
            ],
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'User id'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'User name'
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'User email'
            ],
        ];
    }
}
?>