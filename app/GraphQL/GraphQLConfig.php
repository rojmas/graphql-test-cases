<?php

namespace App\GraphQL;

use App\GraphQL\Mutations\updateUserAvatar;
use App\GraphQL\Mutations\UserMutation;
use App\GraphQL\Queries\PostQuery;
use App\GraphQL\Queries\UsersQuery;
use App\GraphQL\Types\PostType;
use App\GraphQL\Types\UserType;
use App\GraphQL\Mutations\createManufacture;
use App\GraphQL\Mutations\createModelManufacture;
use App\GraphQL\Mutations\createModelManufactureWithImages;
use App\GraphQL\Mutations\deleteManuFacture;
use App\GraphQL\Mutations\deleteModelManufacture;
use App\GraphQL\Mutations\editManuFacture;
use App\GraphQL\Mutations\editModelManufacture;
use App\GraphQL\Mutations\multipleUpload;
use App\GraphQL\Queries\Auth\LoginQuery;
use App\GraphQL\Queries\ManufactureModelQuery;
use App\GraphQL\Queries\ManufactureQuery;
use App\GraphQL\Types\AuthType;
use App\GraphQL\Types\ManufactureModelImages;
use App\GraphQL\Types\ManufactureModelType;
use App\GraphQL\Types\ManufactureType;

// class GraphQLConfig implements ConfigConvertible
class GraphQLConfig
{
    public static function schemas()
    {
        return [

            'default' => [
                'types' => self::$DefaultTypeList,
                'query' => self::$DefaultQueriesList,
                'mutation' => self::$DefaultMutationsList,
                'middleware' => self::$DefaultMiddlewareList,
                'method' => self::$DefaultMethodList,
                'execution_middleware' => self::$DefaultExecutionMiddlewareList,
            ],

            'user' => [
                'types' => self::$UserTypeList,
                'query' => self::$UserQueriesList,
                'mutation' => self::$UserMutationsList,
                'middleware' => self::$UserMiddlewareList,
                'method' => self::$UserMethodList,
                'execution_middleware' => self::$UserExecutionMiddlewareList,
            ],

        ];
    }


    // Use for common Type
    public static array $TypesList = [
        'Auth' => AuthType::class,
        // 'SimpleMessageType' => SimpleMessageType::class,
        \Rebing\GraphQL\Support\UploadType::class,
    ];

    // =========  start the default ========= //

    // Type
    public static array $DefaultTypeList = [
        'user' => UserType::class,
        'post' => PostType::class,
        'manufacture' => ManufactureType::class,
        'manufacture_model' => ManufactureModelType::class,
        'manufacture_model_images' => ManufactureModelImages::class
    ];

    // Queries
    public static array $DefaultQueriesList = [
        'users' => UsersQuery::class,
        'posts' => PostQuery::class,
        'manufactures' => ManufactureQuery::class,
        'manufacture_models' => ManufactureModelQuery::class,
        'login' => LoginQuery::class,
    ];

    // Mutations
    public static array $DefaultMutationsList = [
        'createUsers' => UserMutation::class,
        'createManufacture' => createManufacture::class,
        'editManuFacture' => editManuFacture::class,
        'deleteManuFacture' => deleteManuFacture::class,

        //for the  Model of manufacture with image upload
        'createModelManufacture' => createModelManufacture::class,
        'editModelManufacture' => editModelManufacture::class,
        'deleteModelManufacture' => deleteModelManufacture::class,

        //multiple image upload 
        'createModelManufactureWithImages' => createModelManufactureWithImages::class,
        // 'editManufactureModelWithImages' =>  editManufactureModelWithImages::class,

        //for the users photos upload   
        'updateUserAvatar' => updateUserAvatar::class,
        'multipleUpload' => multipleUpload::class,
    ];

    // Middleware
    public static array $DefaultMiddlewareList = [];

    // Methods
    public static array $DefaultMethodList = ['GET', 'POST'];

    // Execution Middleware
    public static array $DefaultExecutionMiddlewareList = [];

    // =========end of the default ========= //


    // ========= start user ========= //

    // // Type
    public static array $UserTypeList = [
        'User' => UserType::class,
    ];

    // // Queries
    public static array $UserQueriesList = [
        'profile' => UsersQuery::class,
        // 'manufactures' => ManufactureQuery::class,

    ];

    // // Mutations
    public static array $UserMutationsList = [
        'updateUser' => updateUserAvatar::class
    ];

    // // Middleware
    public static array $UserMiddlewareList = [
        'auth:sanctum'
    ];

    // // Methods
    public static array $UserMethodList = ['GET', 'POST'];

    // // Execution Middleware
    public static array $UserExecutionMiddlewareList = [
        \Rebing\GraphQL\Support\ExecutionMiddleware\UnusedVariablesMiddleware::class
    ];

    // ========= end user ===============
}
