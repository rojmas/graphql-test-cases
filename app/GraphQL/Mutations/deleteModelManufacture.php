<?php

namespace App\GraphQL\Mutations;

use App\Models\Manufactor;
use App\Models\ManufactorModel;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class deleteModelManufacture extends Mutation
{
    protected $attributes = [
        'name' => 'Delete The Manufacture Model'
    ];

    public function type(): Type
    {
        return Type::nonNull(GraphQL::type('manufacture_model'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, array $args)
    {
        $manuFactorData = ManufactorModel::find($args['id']);
        if (!$manuFactorData) {
            return null;
        }
        $manuFactorData->delete();
        // dd($manuFactorData);
        return $manuFactorData;
    }
}
