<?php

namespace App\GraphQL\Mutations;

use App\Models\Manufactor;
use Closure;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class deleteManuFacture extends Mutation
{
    protected $attributes = [
        'name' => 'Delete The Manufacture'
    ];

    public function type(): Type
    {
        return Type::nonNull(GraphQL::type('manufacture'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, array $args)
    {
        $manuFactorData = Manufactor::find($args['id']);
        if (!$manuFactorData) {
            return null;
        }
// dd("hiiii hardik");
        $manuFactorData->delete();
        // dd($manuFactorData);
        return $manuFactorData;
    }
}
