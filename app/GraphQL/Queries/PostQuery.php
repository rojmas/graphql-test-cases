<?php 
namespace App\GraphQL\Queries;

use App\Models\Post;
use App\Models\User;
use Closure;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class PostQuery extends Query
{
    protected $attributes = [
        'name' => 'Post Query',
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('post'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id', 
                'type' => Type::string(),
            ],
            
        ];
    }

    public function resolve($root, array $args)
    {
        if (isset($args['id'])) {
            return Post::where('id' , $args['id'])->get();
        }

        return Post::all();
    }
}