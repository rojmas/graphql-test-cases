<?php

namespace App\GraphQL\Queries;

use App\Models\Manufactor;
use App\Models\User;
use Closure;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class ManufactureQuery extends Query
{
    protected $attributes = [
        'name' => 'manufacturesd',
    ];

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('manufacture'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string(),
            ]
        ];
    }

    public function resolve($root, array $args)
    {
        if (isset($args['id'])) {
            return Manufactor::where('id', $args['id'])->get();
        }

        if (isset($args['name'])) {
            return Manufactor::where('name', $args['name'])->get();
        }

        return Manufactor::all();
    }
}
