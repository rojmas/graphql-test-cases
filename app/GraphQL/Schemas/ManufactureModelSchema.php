<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\Mutations\createModelManufacture;
use App\GraphQL\Mutations\deleteModelManufacture;
use App\GraphQL\Mutations\editModelManufacture;
use App\GraphQL\Queries\ManufactureModelQuery;
use App\GraphQL\Types\ManufactureModelType;
use Rebing\GraphQL\Support\Contracts\ConfigConvertible;

class ManufactureModelSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                'manufacture_models' => ManufactureModelQuery::class,
            ],
            'mutation' => [
                'createModelManufacture' => createModelManufacture::class,
                'editModelManufacture' => editModelManufacture::class,
                'deleteModelManufacture' => deleteModelManufacture::class,
            ],
            'types' => [
                'manufacture_model' => ManufactureModelType::class,
            ],
            'middleware' => null,
            'method' => ['GET', 'POST'],
            'execution_middleware' => null,
        ];
    }
}
