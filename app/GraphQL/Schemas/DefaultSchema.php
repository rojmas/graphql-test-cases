<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\Mutations\updateUserAvatar;
use App\GraphQL\Mutations\UserMutation;
use App\GraphQL\Queries\PostQuery;
use App\GraphQL\Queries\UsersQuery;
use App\GraphQL\Types\PostType;
use App\GraphQL\Types\UserType;
use Rebing\GraphQL\Support\Contracts\ConfigConvertible;
use App\GraphQL\Mutations\createManufacture;
use App\GraphQL\Mutations\createModelManufacture;
use App\GraphQL\Mutations\createModelManufactureWithImages;
use App\GraphQL\Mutations\deleteManuFacture;
use App\GraphQL\Mutations\deleteModelManufacture;
use App\GraphQL\Mutations\editManuFacture;
use App\GraphQL\Mutations\editModelManufacture;
use App\GraphQL\Mutations\multipleImageUpload;
use App\GraphQL\Mutations\multipleUpload;
use App\GraphQL\Queries\ManufactureModelQuery;
use App\GraphQL\Queries\ManufactureQuery;
use App\GraphQL\Types\ManufactureModelImages;
use App\GraphQL\Types\ManufactureModelType;
use App\GraphQL\Types\ManufactureType;
use Illuminate\Http\Request;
class DefaultSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                'users' => UsersQuery::class,
                'posts' => PostQuery::class,
                'manufactures' => ManufactureQuery::class,
                'manufacture_models' => ManufactureModelQuery::class,
            ],
            'mutation' => [
                'createUsers' => UserMutation::class,
                'createManufacture' => createManufacture::class,
                'editManuFacture' => editManuFacture::class,
                'deleteManuFacture' => deleteManuFacture::class,

                //for the  Model of manufacture with image upload
                'createModelManufacture' => createModelManufacture::class,
                'editModelManufacture' => editModelManufacture::class,
                'deleteModelManufacture' => deleteModelManufacture::class,

                //multiple image upload 
                'createModelManufactureWithImages' => createModelManufactureWithImages::class,
                // 'editManufactureModelWithImages' =>  editManufactureModelWithImages::class,

                //for the users photos upload   
                'updateUserAvatar' => updateUserAvatar::class,
                'multipleUpload' => multipleUpload::class,
            ],
            // The types only available in this schema
            'types' => [
                'user' => UserType::class,
                'post' => PostType::class,
                'manufacture' => ManufactureType::class,
                'manufacture_model' => ManufactureModelType::class,
                'manufacture_model_images' => ManufactureModelImages::class
            ],

            // Laravel HTTP middleware
            'middleware' => null,

            // Which HTTP methods to support; must be given in UPPERCASE!
            'method' => ['GET', 'POST'],

            // An array of middlewares, overrides the global ones
            'execution_middleware' => null,
        ];
    }
}
