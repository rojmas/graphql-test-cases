<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\Mutations\createManufacture;
use App\GraphQL\Mutations\deleteManuFacture;
use App\GraphQL\Mutations\editManuFacture;
use App\GraphQL\Queries\ManufactureQuery;
use App\GraphQL\Types\ManufactureType;
use Rebing\GraphQL\Support\Contracts\ConfigConvertible;

class ManufactureSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                'manufactures' => ManufactureQuery::class,
            ],
            'mutation' => [
                'createManufacture' => createManufacture::class,
                'editManuFacture' => editManuFacture::class,
                'deleteManuFacture' => deleteManuFacture::class,
            ],
            // The types only available in this schema
            'types' => [
                'manufacture' => ManufactureType::class,
            ],
            // Laravel HTTP middleware
            'middleware' => null,

            // Which HTTP methods to support; must be given in UPPERCASE!
            'method' => ['GET', 'POST'],

            // An array of middlewares, overrides the global ones
            'execution_middleware' => null,
        ];
    }
}
