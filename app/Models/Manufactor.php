<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manufactor extends Model
{
    use HasFactory;

    protected $table = 'manufactors';

    protected $fillable = ['name','description'];

    public function manufacture_models()
    {
        return $this->hasMany(ManufactorModel::class,'manufacture_id','id');
    }
}
