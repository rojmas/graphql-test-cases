<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManufactureModelImages extends Model
{
    use HasFactory;
    protected $table = 'manufacture_model_img';
    protected $fillable = ['manufacture_id', 'path'];

    public function getPathAttribute($value)
    {
        return url('storage/' . $value);
    }
}
