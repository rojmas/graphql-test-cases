<?php

namespace Database\Factories;

use App\Models\Manufactor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ManufactorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Manufactor::class;
    public function definition()
    {
        $name = ['Tata', 'Relience', 'Elon Musk', 'Apple'];
        
        return [
            'name' => $this->faker->randomElements($name,1,false),
            'description' => $this->faker->sentence(20)
        ];
    }
}
