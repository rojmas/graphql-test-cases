<?php

namespace Database\Factories;

use App\Models\Manufactor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ManufactorModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $is_active = ['y', 'n'];
        return [
            'manufacture_id' =>  $this->faker->randomElement(Manufactor::all())['id'],
            'model_name' => $this->faker->word(),
            'model_description' => $this->faker->sentence(20),
            'model_img' => "",
            'is_active' => $this->faker->randomElement($is_active),
        ];
    }
}
